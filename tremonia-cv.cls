\ProvidesClass{tremonia-cv}
\NeedsTeXFormat{LaTeX2e}

% ------------------------------------------------------------------------------
%                                Class options
% ------------------------------------------------------------------------------

% \DeclareOption{draft}{\setlength\overfullrule{5pt}}
% \DeclareOption{final}{\setlength\overfullrule{0pt}}

\DeclareOption*{%
    \PassOptionsToClass{\CurrentOption}{article}
}
\ProcessOptions\relax
\LoadClass{article}

%-------------------------------------------------------------------------------
%                3rd party packages
%-------------------------------------------------------------------------------
% Needed to make header & footer effeciently
\RequirePackage{fancyhdr}
% Needed to manage colors
\RequirePackage{xcolor}
% Needed to configure page layout
\RequirePackage{geometry}
% Needed to use \if-\then-\else statement
\RequirePackage{xifthen}
% Needed for the photo ID
\RequirePackage[skins]{tcolorbox}
% Needed to handle text alignment
\RequirePackage{ragged2e}

\RequirePackage[quiet]{fontspec}
% Needed to deal hyperlink
\RequirePackage[hidelinks,unicode]{hyperref}
% Needed to use icons from font-awesome
\RequirePackage{fontawesome}
% Change background layout
\RequirePackage{background}
% make itemize fancier
\RequirePackage{enumitem}


%-------------------------------------------------------------------------------
%                Configuration for colors
%-------------------------------------------------------------------------------

\definecolor{accent}{HTML}{FF0000}
\definecolor{white}{HTML}{FFFFFF}
\definecolor{black}{HTML}{000000}
\definecolor{gray}{HTML}{5D5D5D}

\colorlet{laccent}{accent!20}
\colorlet{daccent}{accent!70!black}
\colorlet{lgray}{gray!70}
\colorlet{dgray}{gray!70!black}


%-------------------------------------------------------------------------------
%                Configuration for layout
%-------------------------------------------------------------------------------
%% Page Layout
% Configure page margins with geometry
\geometry{
    left=2.0cm, 
    top=1.5cm, 
    right=2.0cm, 
    bottom=2.0cm, 
    footskip=.5cm
}

%% Header & Footer
% Set offset to each header and footer
\fancyhfoffset{0em}
% Remove head rule
\renewcommand{\headrulewidth}{0pt}
% Clear all header & footer fields
\fancyhf{}
% Enable if you want to make header or footer using fancyhdr
\pagestyle{fancy}

\backgroundsetup{
	scale=1,
	pages=all,
	angle=90,
	contents = {%
		\tikz \fill [color=laccent] rectangle (\paperheight,
            \oddsidemargin+1in+\hoffset+\headerphotowidth);
	},
	position=current page.west,
	nodeanchor=north
}

%-------------------------------------------------------------------------------
%                Configuration for fonts
%-------------------------------------------------------------------------------

\newcommand*{\fontdir}[1]{\def\@fontdir{#1}}
\def\@fontdir{fonts/}

\newfontfamily{\fontRegular}[
    Path=\@fontdir,
    UprightFont=*-Regular,
    ItalicFont=*-Italic,
    BoldFont=*-Bold,
]{OpenSans}

\newfontfamily{\fontLight}[
    Path=\@fontdir,
    UprightFont=*-Light,
    ItalicFont=*-LightItalic,
    BoldFont=*-SemiBold,
]{OpenSans}

\newfontfamily{\fontCondensed}[
    Path=\@fontdir,
    UprightFont=*-Light,
    ItalicFont=*-LightItalic,
    BoldFont=*-Bold,
]{OpenSansCondensed}

%-------------------------------------------------------------------------------
%                Commands for elements of CV structure
%-------------------------------------------------------------------------------

\newcommand{\photo}[2][circle,edge]{%
    \def\@photo{#2}
    \@for\tmp:=#1\do{%
        \ifthenelse{\equal{\tmp}{circle} \or \equal{\tmp}{rectangle}}%
        {\let\@photoshape\tmp}{}%
        \ifthenelse{\equal{\tmp}{edge} \or \equal{\tmp}{noedge}}%
        {\let\@photoedge\tmp}{}%
    }%
}

\newcommand*{\drawphoto}{%
    \newlength{\photodim}
    \ifthenelse{\equal{\@photoshape}{circle}}%
    {\setlength{\photodim}{1.3cm}}%
    {\setlength{\photodim}{1.8cm}}%
    \ifthenelse{\equal{\@photoedge}{edge}}%
    {\def\@photoborder{dgray}}%
    {\def\@photoborder{none}}%
    \begin{tikzpicture}%
        \node[\@photoshape, draw=\@photoborder, line width=0.7mm, inner
            sep=\photodim, fill overzoom image=\@photo] (0,0) {};
    \end{tikzpicture}
}

% Define a header for CV
% Usage: \makecvheader
\newcommand*{\makecvheader}[1][C]{%
    % set length in dependence if photo is used
    \newlength{\headertextwidth}
    \newlength{\headerphotowidth}
    \setlength{\headertextwidth}{0.65\textwidth}
    \setlength{\headerphotowidth}{0.30\textwidth}

    % left photo side
    \begin{minipage}[c]{\headerphotowidth}%
        \begin{centering}
            \drawphoto%
        \end{centering}
    \end{minipage}
    \hfill
    % right photo side
    \begin{minipage}[c]{\headertextwidth}%
        \raggedleft
        % \centering
        \headerfirstnamestyle{
            \@firstname 
        }
        \headerlastnamestyle{
            \@lastname
        }\\%
        \ifthenelse{
        \isundefined{\@position}}{}
        {
            \headerpositionstyle{\@position}\\
        }%
        \headeraddressstyle{
            \@address
        }\\%
        \headersocialstyle{%
            \newbool{isstart}%
            \setbool{isstart}{true}%
            \ifthenelse{\isundefined{\@mobile}}
            {}{%
                \faMobile\acvHeaderIconSep\@mobile%
                \setbool{isstart}{false}%
            }%
            \ifthenelse{\isundefined{\@email}}
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \href{mailto:\@email}{\faEnvelope\acvHeaderIconSep\@email}%
            }%
            \ifthenelse{\isundefined{\@homepage}}%
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \href{http://\@homepage}{\faHome\acvHeaderIconSep\@homepage}%
            }%
            \ifthenelse{\isundefined{\@github}}%
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \href{https://github.com/\@github}{\faGithubSquare\acvHeaderIconSep\@github}%
            }%
            \ifthenelse{\isundefined{\@gitlab}}%
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \href{https://gitlab.com/\@gitlab}{\faGitlab\acvHeaderIconSep\@gitlab}%
            }%
            \ifthenelse{\isundefined{\@linkedin}}%
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \href{https://www.linkedin.com/in/\@linkedin}{\faLinkedinSquare\acvHeaderIconSep\@linkedin}%
            }%
            \ifthenelse{\isundefined{\@xing}}%
            {}{%
              \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
              \href{https://www.xing.com/profile/\@xing}{\faXingSquare\acvHeaderIconSep\@xing}
            }%
            \ifthenelse{\isundefined{\@extrainfo}}%
            {}{%
                \ifbool{isstart}{\setbool{isstart}{false}}{\acvHeaderSocialSep}%
                \@extrainfo%
            }%
        }
    \end{minipage}
}

\newcommand*{\makecvbody}[3][\baselineskip]{%
    % set length in dependence if photo is used
    % left photo side
    \begin{minipage}[t]{\headerphotowidth}%
        \vspace{#1}
        #3
    \end{minipage}
    \hfill
    % right photo side
    \begin{minipage}[t]{\headertextwidth}
        \vspace{#1}
        #2
    \end{minipage}%
}

\newcommand{\historysection}[3][none]{%
    \ifthenelse{%
        \equal{#1}{none}%
    }{}{%
        \datestyle{#1}%
    }%
    \hfill
    \sectionstyle{#2} \\
    \ifthenelse{%
        \equal{#3}{none}%
    }{}{%
        \raggedleft
        \notestyle{#3}\\
    }%
    \vspace{0.5cm}
}

\newcommand{\itemsection}[2]{%
    \itemheadingstyle{#1}
    \itemstyle{
            \begin{description}[
                itemsep=0.1cm, 
                itemindent=-0.3cm,
                ]%
            #2
    \end{description} 
    }
    \vspace{1cm}
}

%-------------------------------------------------------------------------------
%                Commands for personal information
%-------------------------------------------------------------------------------
\newcommand*{\name}[2]{\def\@firstname{#1}\def\@lastname{#2}}
\newcommand*{\firstname}[1]{\def\@firstname{#1}}
\newcommand*{\lastname}[1]{\def\@lastname{#1}}
\newcommand*{\address}[1]{\def\@address{#1}}
\newcommand*{\position}[1]{\def\@position{#1}}
\newcommand*{\mobile}[1]{\def\@mobile{#1}}
\newcommand*{\email}[1]{\def\@email{#1}}
\newcommand*{\homepage}[1]{\def\@homepage{#1}}
\newcommand*{\github}[1]{\def\@github{#1}}
\newcommand*{\gitlab}[1]{\def\@gitlab{#1}}
\newcommand*{\linkedin}[1]{\def\@linkedin{#1}}
\newcommand*{\xing}[1]{\def\@xing{#1}}
\newcommand*{\extrainfo}[1]{\def\@extrainfo{#1}}

\newcommand{\acvHeaderSocialSep}{\quad\textbar\quad}
\newcommand{\acvHeaderAfterSocialSkip}{6mm}
\newcommand{\acvHeaderIconSep}{\space}

%-------------------------------------------------------------------------------
%                Configuration for styles
%-------------------------------------------------------------------------------
% Configure styles for each CV elements
% For fundamental structures
\def\@notecolor#1{%
    {\fontsize{20pt}{1em}\fontLight\color{daccent} #1}%
}
\newcommand*{\headerfirstnamestyle}[1]{%
    {\fontsize{32pt}{1em}\fontLight\color{gray} #1}%
}
\newcommand*{\headerlastnamestyle}[1]{%
    {\fontsize{32pt}{1em}\fontRegular\color{dgray} #1}%
}
\newcommand*{\headerpositionstyle}[1]{%
    {\fontsize{10.6pt}{1em}\fontLight\color{accent} #1}%
}
\newcommand*{\headeraddressstyle}[1]{%
    {\fontsize{8pt}{1em}\fontLight\itshape\color{lgray} #1}%
}
\newcommand*{\headersocialstyle}[1]{
    {\fontsize{6.8pt}{1em}\fontRegular\color{gray} #1}%
}
\newcommand*{\sectionstyle}[1]{
    {\fontsize{16pt}{1em}\fontRegular\color{dgray} #1}
}
\newcommand*{\datestyle}[1]{
    {\fontsize{14pt}{1em}\fontCondensed\color{lgray} #1}
}
\newcommand*{\notestyle}[1]{
    {\fontsize{14pt}{1em}\fontLight\itshape\color{gray} #1}
}
\newcommand*{\itemheadingstyle}[1]{
    {\fontsize{16pt}{1em}\fontRegular\color{dgray}\@notecolor #1}
}
\newcommand*{\itemstyle}[1]{
    {\fontsize{12pt}{1em}\fontRegular\color{gray} #1}
}

% vim:ft=tex
